# TRANSIT PLANNER

## To Run 
 
 npm start

## run in background 

forever start -c "npm start" ./

## Background Run

root@localhost:/home/user/planner/planner-backend# forever start -c "npm start" ./

## Remove Mongoose Warning

 npm remove mongoose
 
 npm install mongoose@4.10.8 --save


#NGINX Configuration


[//]: # (Comment)

```

upstream web {
    server 127.0.0.1:8080;
    
}

upstream api {
    server 127.0.0.1:3000;
 
}

server {
    listen 80;
    server_name *.tidbits.in node.tidbits.in;
    #include     /etc/nginx/sites-available/cert/certbot.conf;
    
    gzip             on;
    gzip_proxied     any;
    gzip_types
        text/css
        text/javascript
        text/xml
        text/plain
        application/javascript
        application/x-javascript
        application/json;
    gzip_disable     "MSIE [1-6]\.";
 


     location / {

        proxy_pass http://web/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
    location /api {
        proxy_pass http://api/;
    }

}
 server {
    listen 80;
    server_name api.tidbits.in;
    #include     /etc/nginx/sites-available/cert/certbot.conf;
    
    gzip             on;
    gzip_proxied     any;
    gzip_types
        text/css
        text/javascript
        text/xml
        text/plain
        application/javascript
        application/x-javascript
        application/json;
    gzip_disable     "MSIE [1-6]\.";
 
    location / {
        proxy_pass http://api/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }

}

```

 

