﻿/**
 * Created by lou_cifer on 07.02.17.
 */

var express = require('express');
var router = express.Router();
var services = require('../models/services');
/* GET home page. */
router.post('/', function (req, res, next) {

    var _id = req.body.Object.serviceID;
    debugger;
    if (_id == '') {
        return res.json({ success: false, message: 'Missing obligatory parameters' });
    }

    services.findOne({ _id: _id }).exec(function (err, data) {
        if (!data) return res.json({ success: false, message: 'This route is missing!' });
        return res.json({ success: true, message: data });
    });
});

module.exports = router;
