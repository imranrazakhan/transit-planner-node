﻿/**
 * Created by lou_cifer on 15.01.17.
 */
var express = require('express');
var router = express.Router();
var stops = require('../models/stops');

/* GET home page. */
router.post('/', function (req, res, next) {

    var stopId = req.body.Object.stopId;

    if (stopId == '') {
        return res.json({ success: false, message: 'Missing obligatory parameters' });
    }

    stops.findOne({ userID: req.decoded._id, _id: stopId }).exec(function (err, data) {
        if (!data) return res.json({ success: false, message: 'This stop is missing' });
        data.remove(function (err) {
            if (err) {
                return res.json({ success: false, message: 'Something went wrong..' });
            }

            return res.json({ success: true, message: 'Stop has been deleted successfully!' });
        });

    })


});

module.exports = router;
